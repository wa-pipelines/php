# GitLab CI template for PHP

## Questions

[![https://img.shields.io/badge/slack-pipelines_php-purple?logo=slack](https://img.shields.io/badge/slack-pipelines_php-purple?logo=slack)](https://slack.com/intl/en-gb/)

This project implements a GitLab CI/CD template to build, test and analyse your [PHP](https://www.php.net/) projects.

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration)
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.
